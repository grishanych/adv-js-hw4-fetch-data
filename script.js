let filmMainContainer = document.querySelector('.films-list')

let starWarsList = fetch('https://ajax.test-danit.com/api/swapi/films')
starWarsList.then(response => response.json())
    .then(data => {


        data.forEach(element => {
            let div = document.createElement('div');
            let list = document.createElement('ul');
            let listOfHeroes = document.createElement('div');
            let item = document.createElement('li');
            list.textContent = `Episode ${element.id} - ${element.name}`;
            item.textContent = `${element.openingCrawl}`;
            listOfHeroes.classList.add('list-item');
            item.classList.add('item');
            list.classList.add('list');
            div.classList.add('wrapper');
            div.prepend(list);
            list.append(listOfHeroes)
            listOfHeroes.classList.add('list-item');
            div.append(item)
            filmMainContainer.append(div)


            element.characters.forEach(item => {
                let films = fetch(item)
                console.log(films)
                films.then(response => response.json())

                    .then(film => {
                        let itemOfHeroes = document.createElement('li');
                        itemOfHeroes.classList.add('list-heroes');
                        itemOfHeroes.textContent = film.name;
                        list.append(itemOfHeroes);
                        listOfHeroes.classList.remove('list-item');
                        listOfHeroes.append(itemOfHeroes)

                    })

            })

        })
    })